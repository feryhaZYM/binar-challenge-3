const express = require("express");
const app = express();
const port = 3000;
const userRouter = require("./users/route");

//middleware
app.use(express.json());
app.use(express.static("public"));

app.get("/home", (req, res) => {
    return res.sendFile("./public/homepage/index.html");
} )

app.use("/users", userRouter);

app.get("/hello", (req, res) => {
    return res.json({ message: "ohayou sekai!"})
});


app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});