//contoh local module

const tambah = (x, y) => {
    return x + y;
};

const kurang = (x, y) => {
    return x - y;
};

module.exports = {
    tambah,
    kurang,
};
//===============================================================================

//jika mau ekspor semua function bisa dijadikan sebuah class, maka bisa ditulis seperti dibawah sehingga tidak perlu ditulis satu persatu

// class Calculator {
//     tambah = (x, y) => {
//         return x + y;
//     };
    
//     kurang = (x, y) => {
//         return x - y;
//     };
// }
// module.exports = new Calculator();