const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

let bookList = [
  {
    judul: "Menjadi Polisi Dunia",
    pengarang: "Paman Sam",
  },
  {
    judul: "Tumbal Liberalisme",
    pengarang: "Adian Husaini",
  },
  {
    judul: "Gerpolek",
    pengarang: "Tan Malaka",
  },
  {
    judul: "Das Kapital",
    pengarang: "Karl Marx",
  },
  {
    judul: "Penyambung Lidah Rakyat",
    pengarang: "Cindy Adams",
  },
];

let dataUsersList = [
  {
    username: "admin",
    email: "admin@gmail.com",
    password: "pw123",
  },
];

app.get("/", function (req, res) {
  res.send("Ohayo Sekai!");
});

//API untuk mendapatkan semua daftar buku(book list)
app.get("/books", function (req, res) {
  //cara pertama untuk mendapatkan data dengan query (lebih sering digunakan karena kebanyakan mencari data menggunakan query)
  const queryData = req.query;
  if (queryData.judul || queryData.pengarang) {
    const urutanData = bookList.find((book) => {
      return (book.judul === queryData.judul || book.pengarang === queryData.pengarang);
    });
    if (urutanData === undefined) {
      res.statusCode = 404;
      return res.json({ message: "Buku tidak tersedia cuy!" });
    } else {
      return res.json(urutanData);
    };
  } else {
    res.json(bookList);
  };
});

//API untuk menambahkan buku baru & cara untuk mendapatkan data dengan request body (menggunakan 2 API dalam satu method lebih baik dihindari)
app.post("/books", function (req, res) {
  const data = req.body;
  bookList.push(
    {
      judul: data.judul,
      pengarang: data.pengarang,
    }
  );
  res.json({ message: "sukses menambahkan buku baru!" });
});
  //cara kedua untuk mendapatkan data dengan request body
  // const urutanData = bookList.find((book) => {
  //   return book.judul === dataBody.judul;
  // });
  // if (urutanData === undefined) {
  //   res.statusCode = 404;
  //   return res.json({ message: "Buku tidak tersedia coy!" });
  // }
  // res.json(urutanData);
  // API untuk menambahkan buku baru



//API untuk mendapatkan semua data user
app.get("/users", function (req, res) {
  res.json(dataUsersList);
});

//API untuk menambahkan data user
app.post("/sign-up", function (req, res) {
  const dataUser = req.body;
  //validasi data (mengecek apakah user memasukkan data dengan benar)
  if ((dataUser.username === undefined || dataUser.username === "") || (dataUser.email === undefined || dataUser === "")) {
    res.statusCode = 400;
    return res.json({ message: "Username or Email is invalid, entry data properly!"})
  } else if (dataUser.password === undefined || dataUser.password === "") {
    res.statusCode = 400;
    return res.json({ message: "Entry password properly!"})
  };

  //mengecek data apakah sudah tersedia
  const availableData = dataUsersList.find((data) => {
    return (data.username === dataUser.username || data.email === dataUser.email);
  });
  if (availableData) {
    return res.json({message:"Username or email is available!"});
  };

  //menambahkan baru data ke dalam dataUsersList
  dataUsersList.push({
    username: dataUser.username,
    email: dataUser.email,
    password: dataUser.password,
  });
  res.json({ message: "added new user" });
});

// app.get("/login", (req, res) => {
  
// })

//API untuk mengambil data dari dataUsersList
app.get("/login", (req, res) => {
  // const  rquestData = req.body; improvement ->
  const { username, password } = req.body;
  const dataUserLogin = dataUsersList.find((data) => {
    return (data.username === username && data.password === password ); 
  });

  if (dataUserLogin) {
    return res.json(dataUserLogin);
  } else {
    return res.json({message:"pengguna tidak ditemukan"});
  };
});



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
