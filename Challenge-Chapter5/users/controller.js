const userListModel = require('./model');

class UserListController {
    getAllDataUser = (req, res) => {
        const allDataUsers = userListModel.getAllDataUser();
        return res.json(allDataUsers);
    };

    registerDataUser = (req, res) => {
        const {id, username, email, password} = req.body;
        //validasi input data
        if ((username === undefined || username === "") || (email === undefined || email === "")) {
            res.statusCode = 400;
            return res.json({ message: 'Username or Email is invalid, please entry data correctly!'});
        } else if (password === undefined || password === "") {
            res.statusCode = 400;
            return res.json({ message: 'password is invalid, please entry data correctly!'});
        };
    
        //cek registrasi
        const availableData = userListModel.isDataAvailable(username, email);
        if (availableData) {
            return res.json ({ message: 'Username or Email already registered!' })
        };
        
        //menambah data ke dalam variabel dataUserList
        userListModel.addNewData(id, username, email, password);
        return res.json({ message: 'New user is added'});
    };

    loginDataUser = (req, res) => {
        const { email, password } = req.body;
        const dataVerify = userListModel.verifySignIn(email, password);
    
        if (dataVerify) {
            return res.json(dataVerify);
        } else {
            return res.json({ message: 'Email or Password is invalid, enter data correctly!' })
        };
    };
};

module.exports = new UserListController();