const md5 = require("md5");
const dataUserList = [];

class UserListModel {
    //mendapatkan semua data user
    getAllDataUser = () => {
        return dataUserList;
    };

    //metode mengecek apakah data sudah teregistrasi atau belum
    isDataAvailable = (username, email) => {
        const availableData = dataUserList.find((data) => {
            return (data.username === username || data.email === email);
        });

        if (availableData) {
            return true;
        } else {
            return false;
        };
    };

    //method menambah data baru ke dalam variable dataUserList 
    addNewData = (id, username, email, password) => {
        dataUserList.push({
            id: dataUserList.length + 1,
            username: username,
            email: email,
            password: md5(password),
        });
    };

    //method verifikasi login
    verifySignIn = (email, password) => {
        const dataUser = dataUserList.find((data) => {
            return (data.email === email && data.password === md5(password));
        });

        return dataUser;
    };
};

module.exports = new UserListModel();