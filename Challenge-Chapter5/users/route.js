const express = require("express");
const userRouter = express.Router();
const userListController = require("./controller")


//untuk mendapatkan semua data user
userRouter.get("/data-user", userListController.getAllDataUser);

//API Registrasi/sign-up
userRouter.post("/sign-up", userListController.registerDataUser);

//API Login/sign-in
userRouter.post("/sign-in", userListController.loginDataUser);


module.exports = userRouter;