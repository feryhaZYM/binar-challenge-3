//=====================================================================
//CORE MODULE
//module bawaan saat install nodejs

const fs = require("fs");
const { round } = require("mathjs");

try {
    const data = fs.readFileSync("./README.txt", "utf-8")
    console.log(data);
} catch (err) {
    console.error(err);
}

//========================================================================

//LOCAL MODULE
//module yang dibuat sendiri

const { tambah, kurang } = require("./calculator");

const hasilTambah = kurang(10, 12);
console.log(hasilTambah);

//=========================================================================

//THIRD PARTY MODULE / Dependencies / Library
//Module yang ditulis orang lain 
//contoh mathjs

const {sqrt} = require("mathjs"); //model lama/ type= commonjs
// import {sqrt} from "mathjs";   // cara baru/ type=module/ es6

const square = sqrt(9)
console.log(square);

//==========================================================================